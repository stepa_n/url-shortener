import {baseUrl} from '../constants';

export class StatsEntry {
    constructor(shortLink, linkDetails) {
        this.entry = document.createElement('div');
        this.togglerButton = document.createElement('button');
        this.entryBody = document.createElement('div');
        const entryHeader = document.createElement('h3');
        const visitorsInfo = document.createElement('ul');

        this.togglerButton.innerHTML = `${baseUrl}/${shortLink} &darr;`;
        this.togglerButton.className = 'stats__toggler';
        entryHeader.innerHTML = `<div>Source URL:</div> <a href="${linkDetails.url}">${linkDetails.url}</a>`;
        visitorsInfo.innerHTML = '<h4>Visitors: </h4>'

        linkDetails.visitors.forEach((visitor) => {
            const visitorInfo = document.createElement('li');
            visitorInfo.innerHTML =
                `<b>IP:</b> ${visitor.ip}
                 <b>UserAgent:</b> ${visitor.agent}
                 <b>Time:</b> ${new Date(visitor.time)}`;
            visitorsInfo.appendChild(visitorInfo);
        });

        this.entryBody.appendChild(entryHeader);
        this.entryBody.appendChild(visitorsInfo);
        this.entryBody.style = "display: none";

        this.entry.appendChild(this.togglerButton);
        this.entry.appendChild(this.entryBody);

        this.isEntryVisible = false;
        this.togglerButton.addEventListener('click', () => this.toggleEntry())
    }

    toggleEntry() {
        if(this.isEntryVisible) {
            this.entryBody.style = "display: none";
            this.isEntryVisible = false;
        } else {
            this.entryBody.style = "display: block";
            this.isEntryVisible = true;
        }
    }

    render() {
        return this.entry;
    }
}