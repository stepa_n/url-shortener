import {statsUrl} from '../constants';
import {StatsEntry} from './stats-entry';

export class Stats{
    constructor() {
        this.container = document.getElementById('stats__container');
    }

    refresh() {
        fetch(statsUrl)
            .then(data => data.json())
            .then(links => Object.keys(links).forEach(
                link => {
                    const statsEntry = new StatsEntry(link, links[link]);
                    this.container.appendChild(statsEntry.render());
                }
            ))
    }
}