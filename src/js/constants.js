export const baseUrl = 'http://localhost:4444';
export const makeLinkUrl = `${baseUrl}/makeurl`;
export const statsUrl = `${baseUrl}/stats`;

export const brokenLinkError = 'The link is broken... Please use a full URL';