import validUrl from 'valid-url';
import {makeLinkUrl, brokenLinkError} from '../constants';

export class Shortener {
    constructor() {
        this.input = document.getElementById('shortener__input');
        this.button = document.getElementById('shortener__button');
        this.showCase = document.getElementById('shortener__showcase');
        this.button.addEventListener('click', () => this.handleClick());
    }

    handleClick() {
        if(validUrl.isUri(this.input.value)) {
            fetch(`${makeLinkUrl}?url=${this.input.value}`)
                .then(res => res.json())
                .then(res => {
                    this.showCase.innerHTML = `Your link: <a href="${res.url}">${res.url}</a>`;
                });
        } else {
            this.showCase.innerHTML = brokenLinkError;
        }
    }
}