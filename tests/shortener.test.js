const {Shortener} = require('../src/js/index/shortener');
const {brokenLinkError} = require('../src/js/constants');

describe('Shortener', () => {
    document.body.innerHTML =
        `<div class="shortener">
            <a href="stats.html" class="shortener__stats">Statistics &rarr;</a>
            <input type="text" id="urlInput" class="shortener__input" value="" title="urlInput"/>
            <button id="urlButton" class="shortener__button">Shorten URL</button>
            <span id="urlShowCase" class="shortener__showcase"></span>
        </div>`;
    const shortener = new Shortener();
    shortener.input.value = '<alert>xss!</alert>';
    it('should prevent xss scripting', () => {
        shortener.handleClick();
        expect(shortener.showCase.innerHTML).toEqual(brokenLinkError);
    });
});