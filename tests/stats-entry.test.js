import {StatsEntry} from '../src/js/stats/stats-entry';

describe('StatsEntry', () => {
    const links = {
        'foo': {
            url: 'http://foo.com',
            visitors: [
                {ip: 1, agent: 1, time: 1},
                {ip: 2, agent: 2, time: 2},
            ]
        },
        'bar': {
            url: 'http://bar.com',
            visitors: [
                {ip: 1, agent: 1, time: 1}
            ]
        }
    };
    it('should render all visitors', () => {
        const statsEntry = new StatsEntry('foo', links['foo']);
        expect(statsEntry.entryBody.getElementsByTagName('li').length).toEqual(2);
    })
});