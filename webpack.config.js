const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = {
    entry: {
        index: './src/js/index/index.js',
        stats: './src/js/stats/index.js'
    },
    output: {
        path: __dirname + "/dist",
        filename: '[name].js'
    },
    plugins: [
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin([
            {from: '*.html'},
            {from: './src/styles/*.css', to: './styles/[name].[ext]'}
        ])
    ]
};