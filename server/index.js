const express = require('express');
const cors = require('cors');

const host = '0.0.0.0';
const port = 4444;
const app = express();
app.use(cors());

const links = {};

const makeRandom = () => Math.random().toString(36).slice(2, 7);

app.get('/makeurl', (req, res) => {
    const url = req.query.url;

    if(url) {
        const randomShortUrl = makeRandom();
        Object.assign(links, {[randomShortUrl]: {url, visitors: []}});
        res.send({url: `http://${host}:${port}/${randomShortUrl}`});
    } else {
        res.send(400);
    }
});

app.get('/stats', (req, res) => {
    res.send(links);
});

app.get('/:url', (req, res) => {
    const shortUrl = req.params.url;
    if(links[shortUrl]) {
        links[shortUrl].visitors.push({
            ip: req.connection.remoteAddress,
            agent: req.header('user-agent'),
            time: Date.now()
        });
        res.redirect(links[shortUrl].url);
    } else {
        res.status(500);
    }
});

app.listen(port, host, (err) => {
    if (err) {
        console.log(err);
        return;
    }

    console.log(`Listening at http://${host}:${port}`);
});
